package com.beauto.inclusivity.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.beauto.inclusivity.exceptions.NoContentException;
import com.beauto.inclusivity.exceptions.UserNotFoundException;
import com.beauto.inclusivity.model.User;
import com.beauto.inclusivity.repository.UserRepository;

@RestController
@RequestMapping(value = "/api/user", produces = "application/json")
public class UserController {
    @Autowired
    UserRepository userRepository;

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public User createUser(@RequestBody @Valid User user) {
        return userRepository.save(user);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = "application/json", value="{id}")
    public User updateUser(@RequestBody @Valid User user, @PathVariable long id) {
        User userFromDb = null;
        try {
            userFromDb = (User)userRepository.findById(id).get();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        userFromDb.setUserName(user.getUserName());
        userFromDb.setFirstName(user.getFirstName());
        userFromDb.setLastName(user.getLastName());
        return userRepository.save(userFromDb);
    }
    

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List<User> getAllUsers() {
        List<User> userList = new ArrayList<User>();
        userRepository.findAll().forEach(userList::add);
        if (userList.size()==0) {
            throw new NoContentException();
        }
        return userList;
    }
    
    @RequestMapping(method = RequestMethod.DELETE, value="{id}")
    public void deleteUser(@PathVariable long id) {
        User userFromDb = null;
        try {
            userFromDb = (User)userRepository.findById(id).get();
        } catch (Exception e) {
            throw new UserNotFoundException();
        }
        userRepository.delete(userFromDb);  
    }
    
    @RequestMapping(method = RequestMethod.GET, value="{id}")
    public User getUserDetails(@PathVariable long id) {
        User userFromDb = null;
        try {
            userFromDb = (User)userRepository.findById(id).get();
        } catch (Exception e) {
            throw new UserNotFoundException();
        }
        return userFromDb;
    }
}
