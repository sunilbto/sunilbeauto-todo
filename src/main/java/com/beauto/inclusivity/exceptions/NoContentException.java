package com.beauto.inclusivity.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NO_CONTENT, reason = "User not found")
public class NoContentException extends RuntimeException {
}