package com.beauto.inclusivity.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.beauto.inclusivity.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findByUserName(String userName);
    Optional<User> findByFirstName(String firstName);
}