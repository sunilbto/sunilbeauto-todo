package com.beauto.inclusivity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InclusivityApplication {
   public static void main(String[] args) {
      SpringApplication.run(InclusivityApplication.class, args);
   }
}
