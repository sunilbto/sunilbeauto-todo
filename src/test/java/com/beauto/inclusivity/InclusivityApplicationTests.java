package com.beauto.inclusivity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import com.beauto.inclusivity.model.User;
import com.beauto.inclusivity.repository.UserRepository;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InclusivityApplicationTests {
    
    @Value("${local.server.port}")
    protected int serverPort;

    @Autowired
    UserRepository userRepository;

    @Before
    public void setUp() {
        RestAssured.port = serverPort;
    }
    
    @Test
    public void createUserTest() {
        String userName = "Happy";
        String firstName = "Happy";
        String lastName = "Singh";
        User user = new User(userName, firstName, lastName);
        given().body(user).contentType(ContentType.JSON).when().post("/api/user").then()
                .statusCode(equalTo(HttpStatus.OK.value())).body("id", not(nullValue()))
                .body("firstName", is(firstName)).body("lastName", is(lastName)).body("userName", is(userName));
        Optional<User> userOnRecord = userRepository.findByUserName(userName);
        assertEquals(userName,userOnRecord.get().getUserName());
    }

    @Test
    public void createUserFailWithNullValuesTest() {
        String userName = null;
        String firstName = null;
        String lastName = null;
        User user = new User(userName, firstName, lastName);
        given().body(user).contentType(ContentType.JSON).when().post("/api/user").then()
                .statusCode(equalTo(HttpStatus.BAD_REQUEST.value()));
        Optional<User> userOnRecord = userRepository.findByUserName(userName);
        assertFalse(userOnRecord.isPresent());
    }
    
    @Test
    public void updateUserSuccessTest() {
        String userName = "Robin";
        String firstName = "Robin";
        String lastName = "Singh";
        User user = new User(userName, firstName, lastName);
        userRepository.save(user);
        String newFirstName = "Vijay";
        Optional<User> userOnRecord = userRepository.findByUserName(userName);
        User userFromDb = userOnRecord.get();
        userFromDb.setFirstName(newFirstName);
        given().body(userOnRecord).contentType(ContentType.JSON).when().put("/api/user/"+userFromDb.getId()).then()
                .statusCode(equalTo(HttpStatus.OK.value()));
        Optional<User> userFromRecord = userRepository.findByFirstName(newFirstName);
        assertTrue(userFromRecord.isPresent());
    }
    
    @Test
    public void updateUserFailWithNullNameValueTest() {
        String userName = "Tarun";
        String firstName = "Tarun";
        String lastName = "Sagar";
        User user = new User(userName, firstName, lastName);
        userRepository.save(user);
        String newFirstName = null;
        Optional<User> userOnRecord = userRepository.findByUserName(userName);
        User userFromDb = userOnRecord.get();
        userFromDb.setFirstName(newFirstName);
        given().body(userOnRecord).contentType(ContentType.JSON).when().put("/api/user/"+userFromDb.getId()).then()
                .statusCode(equalTo(HttpStatus.BAD_REQUEST.value()));
        Optional<User> userFromRecord = userRepository.findByFirstName(newFirstName);
        assertFalse(userFromRecord.isPresent());
    }
    
    @Test
    public void updateUserFailWithNullIdValueTest() {
        String userName = "Ganesh";
        String firstName = "Ganesh";
        String lastName = "Gokhale";
        User user = new User(userName, firstName, lastName);
        userRepository.save(user);
        String newFirstName = null;
        Optional<User> userOnRecord = userRepository.findByUserName(userName);
        User userFromDb = userOnRecord.get();
        userFromDb.setFirstName(newFirstName);
        given().body(userOnRecord).contentType(ContentType.JSON).when().put("/api/user/1000").then()
                .statusCode(equalTo(HttpStatus.BAD_REQUEST.value()));
        Optional<User> userFromRecord = userRepository.findByFirstName(newFirstName);
        assertFalse(userFromRecord.isPresent());
    }
    

    @Test
    public void getAllUsersTest() {
        User user = new User("Hitesh", "Hitesh", "Rawat");
        User user2 = new User("Ravi", "Ravi", "Gole");
        User user3 = new User("Aman", "Aman", "Teja");
        userRepository.save(user);
        userRepository.save(user2);
        userRepository.save(user3);
        given().get("/api/user").then()
                .statusCode(equalTo(HttpStatus.OK.value()))
                .body("size()", is(3));
    }
    
    @Test
    public void getAllUsersFailTest() {
        given().get("/api/user").then()
                .statusCode(equalTo(HttpStatus.NO_CONTENT.value()));
    }
    
    @Test
    public void getUserDetailsTest() {
    	User user = new User("Pankaj", "Pankaj", "Pandey");
        user = userRepository.save(user);
        System.out.println("User is like : "+user.toString());
        given().pathParam("userID", user.getId()).get("/api/user/{userID}").then()
                .statusCode(equalTo(HttpStatus.OK.value()))
                .body("id", equalTo(Integer.parseInt(Long.toString(user.getId()))));
    }
    
    @Test
    public void getUserDetailsFailWithUserIdTest() {
        given().pathParam("userID", 0).get("/api/user/{userID}").then()
                .statusCode(equalTo(HttpStatus.NOT_FOUND.value()));
    }
    
    @Test
    public void deleteUserTest() {
        User user = new User("Kamal", "Kamal", "Nath");
        user = userRepository.save(user);
        given().pathParam("userID", user.getId())
		.delete("/api/user/{userID}")
		.then().statusCode(equalTo(HttpStatus.OK.value()));	
    }
    
    @Test
    public void deleteUserFailWithIdTest() {
    	given().pathParam("userID", 0)
		.delete("/api/user/{userID}")
		.then().statusCode(equalTo(HttpStatus.NOT_FOUND.value()));	
    }  
}
